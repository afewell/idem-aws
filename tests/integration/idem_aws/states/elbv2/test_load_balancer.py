import copy
import time
from collections import ChainMap
from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-elbv2-lb-" + str(int(time.time())),
    "scheme": "internet-facing",
    "lb_type": "application",
    "ip_address_type": "ipv4",
}
RESOURCE_TYPE = "aws.elbv2.load_balancer"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    cleanup,
    aws_ec2_default_vpc,
    aws_ec2_default_subnets,
    aws_ec2_default_internet_gateway,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test

    assert (
        aws_ec2_default_vpc
        and aws_ec2_default_subnets
        and aws_ec2_default_internet_gateway
    )
    assert aws_ec2_default_vpc.get(
        "default_vpc_id"
    ), f"Test needs default VPC. Default VPC is not found in region : {ctx['acct'].get('region_name')}"
    assert aws_ec2_default_subnets.get(
        "id_a_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'a'}"
    assert aws_ec2_default_subnets.get(
        "id_b_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'b'}"
    assert aws_ec2_default_subnets.get(
        "id_c_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'c'}"
    assert aws_ec2_default_internet_gateway.get(
        "internet_gateway_id"
    ), f"Default VPC needs to have an attached Internet Gateway."
    attributes = [
        {
            "Key": "deletion_protection.enabled",
            "Value": "false",
        },
        {
            "Key": "idle_timeout.timeout_seconds",
            "Value": "60",
        },
    ]
    PARAMETER["default_vpc_id"] = aws_ec2_default_vpc.get("default_vpc_id")
    PARAMETER["id_a_subnet"] = aws_ec2_default_subnets.get("id_a_subnet")
    PARAMETER["id_b_subnet"] = aws_ec2_default_subnets.get("id_b_subnet")
    PARAMETER["id_c_subnet"] = aws_ec2_default_subnets.get("id_c_subnet")
    PARAMETER["internet_gateway_id"] = aws_ec2_default_internet_gateway.get(
        "internet_gateway_id"
    )
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["attributes"] = attributes
    PARAMETER["subnets"] = [
        PARAMETER["id_a_subnet"],
        PARAMETER["id_b_subnet"],
    ]
    response = await hub.states.aws.elbv2.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        subnets=PARAMETER["subnets"],
        tags=PARAMETER["tags"],
        scheme=PARAMETER["scheme"],
        lb_type=PARAMETER["lb_type"],
        ip_address_type=PARAMETER["ip_address_type"],
        attributes=PARAMETER["attributes"],
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert PARAMETER["lb_type"] == resource.get("lb_type")
    assert PARAMETER["scheme"] == resource.get("scheme")
    assert len(resource.get("subnets")) == 2
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))
    assert compare_attributes(attributes, resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-resource_id", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    # This test  is here to avoid creating another ElasticLoadBalancingv2 Load Balancer fixture for exec.get() testing.
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["scheme"] == resource.get("scheme")
    assert PARAMETER["lb_type"] == resource.get("lb_type")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-name", depends=["present"])
async def test_exec_get_by_name(hub, ctx):
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["lb_type"] == resource.get("lb_type")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-load-balancer", depends=["present"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["attributes"] = (
        {
            "Key": "routing.http.x_amzn_tls_version_and_cipher_suite.enabled",
            "Value": "false",
        },
        {
            "Key": "routing.http.xff_client_port.enabled",
            "Value": "false",
        },
        {
            "Key": "idle_timeout.timeout_seconds",
            "Value": "300",
        },
    )
    PARAMETER["subnets"] = [
        PARAMETER["id_a_subnet"],
        PARAMETER["id_b_subnet"],
        PARAMETER["id_c_subnet"],
    ]
    PARAMETER["tags"] = {
        "Name": PARAMETER["name"],
        "Description": "ELBv2 Load Balancer.",
    }
    response = await hub.states.aws.elbv2.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        subnets=PARAMETER["subnets"],
        tags=PARAMETER["tags"],
        scheme=PARAMETER["scheme"],
        lb_type=PARAMETER["lb_type"],
        ip_address_type=PARAMETER["ip_address_type"],
        attributes=PARAMETER["attributes"],
        resource_id=PARAMETER["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert PARAMETER["lb_type"] == resource.get("lb_type")
    assert PARAMETER["scheme"] == resource.get("scheme")
    assert len(resource.get("subnets")) == 3
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))
    assert resource.get("attributes")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-subnets-tags", depends=["update-load-balancer"])
async def test_update_subnets_and_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["subnets"] = [
        PARAMETER["id_b_subnet"],
        PARAMETER["id_c_subnet"],
    ]
    PARAMETER["tags"] = {
        "Description": "ELB Load Balancer. Updated",
        "Summary": "Summary of ELB Load Balancer",
    }
    response = await hub.states.aws.elbv2.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        subnets=PARAMETER["subnets"],
        tags=PARAMETER["tags"],
        resource_id=PARAMETER["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert len(resource.get("subnets")) == 2
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-address-type", depends=["update-subnets-tags"])
async def test_update_address_type(hub, ctx):
    # Update ElasticLoadBalancingV2 load_balancer. Update IpAddressType.
    response = await hub.states.aws.elbv2.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        ip_address_type="dualstack",
        resource_id=PARAMETER["resource_id"],
    )
    assert not response["result"]
    assert (
        "ClientError: An error occurred (ValidationError) when calling the SetIpAddressType operation: "
        "You must specify subnets with an associated IPv6 CIDR block."
        in response["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update_no_changes", depends=["update-subnets-tags"])
async def test_update_with_no_changes(hub, ctx, __test):
    # Update ElasticLoadBalancingV2 load_balancer with no parameters modified to test that no changes are made
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    del new_parameter["default_vpc_id"]
    del new_parameter["id_a_subnet"]
    del new_parameter["id_b_subnet"]
    del new_parameter["id_c_subnet"]
    del new_parameter["internet_gateway_id"]
    ctx["test"] = __test
    response = await hub.states.aws.elbv2.load_balancer.present(ctx, **new_parameter)
    assert response["result"]
    assert response["old_state"] and response["new_state"]
    # asserting no changes to be made if no parameter is modified
    assert not response["changes"]
    assert not response["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update_no_changes"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.elbv2.load_balancer.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.elbv2.load_balancer.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.elbv2.load_balancer.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert PARAMETER["lb_type"] == resource.get("lb_type")
    assert PARAMETER["scheme"] == resource.get("scheme")
    assert len(resource.get("subnets")) == 2
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))
    assert resource.get("attributes")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete AWS ElasticLoadBalancing(ELB) Load Balancer.
    ret = await hub.states.aws.elbv2.load_balancer.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    describe_resource = ret.get("old_state")
    resource = dict(ChainMap(describe_resource))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert PARAMETER["lb_type"] == resource.get("lb_type")
    assert PARAMETER["scheme"] == resource.get("scheme")
    assert len(resource.get("subnets")) == 2
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))
    assert resource.get("attributes")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elbv2.load_balancer.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_lb_absent_with_none_resource_id(hub, ctx):
    # Delete ELBv2 LB with resource_id as None. Result in no-op.
    ret = await hub.states.aws.elbv2.load_balancer.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_lb_absent_with_invalid_resource_id(hub, ctx):
    ret = await hub.states.aws.elbv2.load_balancer.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid-xyz-abc",
    )
    assert not ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeLoadBalancers operation: "
        f"'invalid-xyz-abc' is not a valid load balancer ARN" in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.elbv2.load_balancer.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def compare_attributes(
    input_attributes: List[Dict[str, Any]], output_attributes: List[Dict[str, Any]]
):
    assert output_attributes
    out_attributes_map = {
        attribute.get("Key"): attribute for attribute in output_attributes or []
    }

    for attribute in input_attributes:
        if attribute.get("Key") in out_attributes_map:
            if attribute != out_attributes_map.get(attribute.get("Key")):
                return False
    return True
