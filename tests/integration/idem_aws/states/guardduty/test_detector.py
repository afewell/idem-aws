import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_detector(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    detector_id = None
    tags = {"detector": "my_detector"}
    data_sources = {"S3Logs": {"Enable": True}}
    finding_publishing_frequency = "SIX_HOURS"
    detector_temp_name = "idem-test-detector-" + str(uuid.uuid4())

    # create_detector with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.guardduty.detector.present(
        test_ctx,
        name=detector_temp_name,
        tags=tags,
        data_sources=data_sources,
        finding_publishing_frequency=finding_publishing_frequency,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.guardduty.detector {detector_temp_name}" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert detector_temp_name == resource.get("name")

    # create_detector in real
    ret = await hub.states.aws.guardduty.detector.present(
        ctx, name=detector_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created '{detector_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    # describe_detector
    des = await hub.states.aws.guardduty.detector.describe(
        ctx,
    )
    assert des
    detector_id = resource_id
    assert "aws.guardduty.detector.present" in des.get(detector_id)
    described_resource = des.get(detector_id).get("aws.guardduty.detector.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert tags == described_resource_map.get("tags")
    assert finding_publishing_frequency == described_resource_map.get(
        "finding_publishing_frequency"
    )
    assert all(
        described_resource_map.get("data_sources").get(key, None) == val
        for key, val in data_sources.items()
    )

    # Update detector with test flag
    ret = await hub.states.aws.guardduty.detector.present(
        test_ctx,
        name=detector_temp_name,
        resource_id=resource_id,
        finding_publishing_frequency="ONE_HOUR",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert "ONE_HOUR" == resource.get("finding_publishing_frequency")

    # update_detector in real
    # update finding_publishing_frequency in real
    ret = await hub.states.aws.guardduty.detector.present(
        ctx,
        name=detector_temp_name,
        resource_id=resource_id,
        finding_publishing_frequency="ONE_HOUR",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Update finding_publishing_frequency: ONE_HOUR" in ret["comment"]

    # update data source in real
    data_sources = {"S3Logs": {"Enable": False}}
    ret = await hub.states.aws.guardduty.detector.present(
        ctx,
        name=detector_temp_name,
        resource_id=resource_id,
        data_sources=data_sources,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Update data_sources: {data_sources}" in ret["comment"]

    # already present scenario
    ret = await hub.states.aws.guardduty.detector.present(
        ctx,
        name=detector_temp_name,
        resource_id=resource_id,
        finding_publishing_frequency="ONE_HOUR",
    )
    assert ret["result"], ret["comment"]
    assert hub.tool.aws.guardduty.detector.compare_data_sources(
        ret["old_state"], ret["new_state"]
    )
    assert f"{detector_temp_name} already exists" in ret["comment"]

    # update_tags
    new_tags = {"new_detector": "test_detector"}
    ret = await hub.states.aws.guardduty.detector.present(
        ctx, name=detector_temp_name, resource_id=resource_id, tags=new_tags
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")

    # removing_tags
    tags = {}
    ret = await hub.states.aws.guardduty.detector.present(
        ctx, name=detector_temp_name, resource_id=resource_id, tags=tags
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")
    assert tags == resource.get("tags", {})

    # Delete detector with test flag
    ret = await hub.states.aws.guardduty.detector.absent(
        test_ctx,
        name=detector_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would delete aws.guardduty.detector" in ret["comment"]

    # Delete detector in real
    ret = await hub.states.aws.guardduty.detector.absent(
        ctx,
        name=detector_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted '{detector_temp_name}'" in ret["comment"]

    # Deleting detector again should be a no-op
    ret = await hub.states.aws.guardduty.detector.absent(
        ctx,
        name=detector_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"'{detector_temp_name}' already absent" in ret["comment"]
