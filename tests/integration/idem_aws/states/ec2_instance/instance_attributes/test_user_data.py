import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["user_data"] = "new_value"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("old", {}).get("user_data")
        assert ret["changes"]["new"]["user_data"] == "new_value"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["user_data"] = "changed_value"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["old"]["user_data"] == "new_value"
        assert ret["changes"]["new"]["user_data"] == "changed_value"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["user_data"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new", {}).get("user_data")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
