"""Build plugin metadata which can be used by pop-create for plugin code generation"""
from typing import Any
from typing import Dict

__func_alias__ = {"type_": "type"}


def parse(
    hub,
    ctx,
    resource_name: str,
    shared_resource_data: dict,
) -> Dict[str, Any]:
    """
    Plugin related
    """
    missing_requisites = False

    if ctx.create_plugin == "state_modules":
        missing_requisites = (
            hub.pop_create.aws.plugin.missing_requisites_for_state_modules(
                shared_resource_data
            )
        )
    elif ctx.create_plugin == "auto_state":
        missing_requisites = (
            hub.pop_create.aws.plugin.missing_requisites_for_auto_states(
                shared_resource_data
            )
        )

    if missing_requisites:
        hub.log.info(
            f"The resource {resource_name} is missing requisite for {ctx.create_plugin}"
        )
        return dict()

    present = hub.pop_create.aws.plugin.generate_present(
        resource_name, shared_resource_data
    )
    absent = hub.pop_create.aws.plugin.generate_absent(
        resource_name, shared_resource_data
    )
    describe = hub.pop_create.aws.plugin.generate_describe(
        resource_name, shared_resource_data
    )

    get = hub.pop_create.aws.plugin.generate_get(resource_name, shared_resource_data)

    plugin = {
        "doc": "",
        "imports": [
            "import copy",
            "from dataclasses import field",
            "from dataclasses import make_dataclass",
            "from typing import *",
        ],
        "virtualname": resource_name,
        "functions": {
            "present": present,
            "absent": absent,
            "describe": describe,
            "get": get,
            "create": present,
            "update": present,  # TODO: should it really be this?
            "delete": absent,
            "list": describe,
        },
    }

    if ctx.create_plugin == "auto_state":
        plugin["contracts"] = ["auto_state", "soft_fail"]
    elif ctx.create_plugin == "state_modules":
        plugin["contracts"] = ["resource"]

    return plugin


def missing_requisites_for_state_modules(hub, shared_resource_data: dict) -> bool:
    # If the resource has no list/create/delete call
    required_operations = {
        r: shared_resource_data[r]
        for r in ["create", "delete", "list"]
        if r in shared_resource_data
    }
    return any(len(value) == 0 for value in required_operations.values())


def missing_requisites_for_auto_states(hub, shared_resource_data: dict) -> bool:
    # If the resource has no get/list/create/delete call
    required_operations = {
        r: shared_resource_data[r]
        for r in ["get", "create", "delete", "list"]
        if r in shared_resource_data
    }
    return any(len(value) == 0 for value in required_operations.values())


def generate_describe(hub, resource_name, shared_resource_data):
    describe_function_definition = shared_resource_data["list"]
    return {
        "doc": f"List all {resource_name} resources for the given account. \n{describe_function_definition.get('doc', '')}",
        "hardcoded": dict(
            parameter=describe_function_definition.get("params", {}),
            **describe_function_definition.get("hardcoded", {}),
        ),
    }


def generate_present(hub, resource_name, shared_resource_data):
    create_function_definition = shared_resource_data["create"]
    return {
        "doc": f"{create_function_definition.get('doc', '')}",
        "hardcoded": dict(
            parameter=dict(
                resource_id=hub.pop_create.aws.plugin.get_resource_id_param(),
                **create_function_definition.get("params", {}),
            ),
            **create_function_definition.get("hardcoded", {}),
        ),
    }


def generate_absent(hub, resource_name, shared_resource_data):
    delete_function_definition = shared_resource_data["create"]
    return {
        "doc": f"{delete_function_definition.get('doc', '')}",
        "hardcoded": dict(
            parameter=dict(
                resource_id=hub.pop_create.aws.plugin.get_resource_id_param(),
                **delete_function_definition.get("params", {}),
            ),
            **delete_function_definition.get("hardcoded", {}),
        ),
    }


def generate_get(hub, resource_name, shared_resource_data):
    get_function_definition = shared_resource_data["get"]
    return {
        "doc": f"Get {resource_name} resources for the given account. \n{get_function_definition.get('doc', '')}",
        "hardcoded": dict(
            parameter=get_function_definition.get("params", {}),
            **get_function_definition.get("hardcoded", {}),
        ),
    }


def get_resource_id_param(hub):
    return hub.pop_create.aws.template.RESOURCE_ID_PARAMETER.copy()
