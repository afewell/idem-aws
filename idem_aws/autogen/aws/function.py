"""Read operation's metadata and extract function definition"""
import botocore.client
import botocore.docs.docstring


def parse(
    hub,
    session: "boto3.session.Session",
    aws_service_name,
    resource_name,
    func_name: str,
):
    client = session.client(service_name=aws_service_name, region_name="us-west-2")

    function = getattr(client, func_name)
    doc: botocore.docs.docstring.ClientMethodDocstring = function.__doc__
    docstring = hub.tool.format.html.parse(doc._gen_kwargs["method_description"])

    function_doc = "\n".join(hub.tool.format.wrap.wrap(docstring, width=112))
    is_idempotent = "idempotent" in function_doc.lower()

    # Parse request params
    parameters = hub.pop_create.aws.function.resolve_request_params(
        aws_service_name, func_name, doc
    )
    hub.pop_create.aws.function.normalize_name_param(parameters, is_idempotent)

    # Parse response
    return_type, return_fields = hub.pop_create.aws.function.resolve_response_metadata(
        aws_service_name, func_name, doc
    )

    # Build function definition
    ret = {
        "doc": function_doc,
        "params": parameters,
        "hardcoded": {
            "aws_service_name": aws_service_name,
            "resource_name": resource_name,
            "function": func_name,
            "boto3_function": f"hub.exec.boto3.client.{aws_service_name}.{resource_name}.{func_name}",
            "return_type": return_type,
            "return_fields": return_fields,
            "has_client_token": bool(parameters.get("ClientToken", None)),
            "is_idempotent": is_idempotent,
        },
    }

    return ret


def resolve_request_params(
    hub,
    aws_service_name,
    func_name,
    doc: "botocore.docs.docstring.ClientMethodDocstring",
):
    parameters = {}
    try:
        params = doc._gen_kwargs["operation_model"].input_shape.members
        required_params = doc._gen_kwargs[
            "operation_model"
        ].input_shape.required_members

        for p, data in params.items():
            # Unwrap TagSpecification and get tags
            if "TagSpecifications" in p:
                p = "tags"
                data = data.member.members["Tags"]
            elif "DryRun" in p:
                # skip "DryRun" as idem-aws don't make use of it
                continue
            parameters[p] = hub.pop_create.aws.param.parse(
                param=data, required=p in required_params, parsed_nested_params=[]
            )
    except AttributeError as e:
        hub.log.error(
            f"Error reading parameters for {aws_service_name}.{func_name}: {e}"
        )

    return parameters


def normalize_name_param(hub, parameters: dict, is_idempotent: bool):
    # Normalize the name parameter
    if "Name" in parameters:
        name = parameters.pop("Name")
    elif "name" in parameters:
        name = parameters.pop("name")
    else:
        name = hub.pop_create.aws.template.NAME_PARAMETER.copy()
        if is_idempotent:
            name["doc"] = "The name of the Idem state"

    parameters["Name"] = name


def resolve_response_metadata(
    hub,
    aws_service_name,
    func_name,
    doc: "botocore.docs.docstring.ClientMethodDocstring",
):
    response_metadata = doc._gen_kwargs["operation_model"].output_shape

    if response_metadata is None:
        return None, {}

    return_type = None
    return_fields = {}
    try:
        return_type = hub.pop_create.aws.param.type(response_metadata)
    except AttributeError as e:
        hub.log.error(
            f"Error reading return type for {aws_service_name}.{func_name}: {e}"
        )

    try:
        response_params = response_metadata.members
        for p, data in response_params.items():
            return_fields[p] = dict(
                name=p, doc=hub.tool.format.html.parse(data.documentation)
            )
    except AttributeError as e:
        hub.log.error(
            f"Error reading return fields for {aws_service_name}.{func_name}: {e}"
        )

    return return_type, return_fields
